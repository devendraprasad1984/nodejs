import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

const UserType = new GraphQLObjectType({
  name: "user",
  fields: () => ({
    id: { type: GraphQLInt },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    email: { type: GraphQLString },
    password: { type: GraphQLString },
  }),
});

// module.exports = UserType;
export default UserType
