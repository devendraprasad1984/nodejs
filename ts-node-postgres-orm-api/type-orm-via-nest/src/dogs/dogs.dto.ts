export class DogsDto{
    readonly name: string
    readonly age: number
    readonly breed?: string
}
